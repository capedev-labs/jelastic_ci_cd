Create docker images and deploy them on a Jelastic server:
* the project path (group/project_name) will be used as the main identifier for the docker name and Jelastic env name
* the master branch will create docker image with tag _latest_
* A prod environment will be created or updated in jelastic ( in the environment group called prod)
* other branchs will create docker images with _branch name_ as tag and be deployed on intg environment groups. 

**Warning**: Didn't find a solution to clean these environments if the branch is deleted (no webhook :( )

# Required environment variables( should be defined in the project or group)

Defined in the project or group variables (settings > CI/CD > Variables): 
* JELASTIC_URL  "https://app.hidora.com"
* JELASTIC_DASHBOARD_APPID  "1dd8d191d38fff45e62564fcf67fdcd6"
* JELASTIC_LOGIN your jelastic login. 
* JELASTIC_PWD your jelastic password. 
* DOCKER_REGISTRY_URL the registry (ex: registry.gitlab.com).
* DOCKER_REGISTRY_USER user for the registry
* DOCKER_REGISTRY_PWD password for the registry. Use a token created from your [profile](https://gitlab.com/profile/personal_access_tokens)


See [install.sh](https://gitlab.com/capedev-labs/jelastic_ci_cd/blob/master/scripts_deploy/install.sh) to have more details on Jelastic API.


# Nodes
By default, the environment will have 2 dockers nodes in order to use sequential re-deployment.
Nodes are defined in the file [nodes.json](https://gitlab.com/capedev-public/devops/blob/master/jelastic_docker/scripts_deploy/nodes.json)

# Main links

* [Docker images](https://gitlab.com/capedev-labs/jelastic_ci_cd/container_registry)
* [CI/CD](https://gitlab.com/capedev-labs/jelastic_ci_cd/settings/ci_cd)


# Jelastic results

with a branch "new_feature":
![Result in Hidora](docs/hidora_result.jpg)

# TODO

* delete environment if a branch is deleted
