#!/usr/bin/env bash
# script to deploy on Jelastic from gitlab
#params are: "environment name" "docker tag" "environment groups"
ENV_NAME=${1}
DOCKER_IMAGE_TAG=${2}
ENV_GROUPS=${3}

#Required environment variables are:
# JELASTIC_URL  "https://app.hidora.com"
# JELASTIC_DASHBOARD_APPID  "1dd8d191d38fff45e62564fcf67fdcd6"
# JELASTIC_LOGIN your jelastic login. Defined in the project or group variables (settings > CI/CD > Variables)
# JELASTIC_PWD your jelastic password. Defined in the project or group variables (settings > CI/CD > Variables)
# DOCKER_REGISTRY_URL the registry (registry.gitlab.com). Defined in the project or group variables (settings > CI/CD > Variables)
# DOCKER_REGISTRY_PWD password for the registry. Defined in the project or group variables (settings > CI/CD > Variables)
# DOCKER_REGISTRY_USER user for the registry
# DOCKER_IMAGE_NAME the name of the image ( could be $CI_PROJECT_PATH for instance)

echo "Env Name   = ${ENV_NAME}";
echo "Docker tag = ${DOCKER_IMAGE_TAG}";
echo "Env Groups = ${ENV_GROUPS}";

CONTENT_TYPE="Content-Type: application/x-www-form-urlencoded; charset=UTF-8;";
USER_AGENT="Mozilla/4.73 [en] (X11; U; Linux 2.2.15 i686)"

warn () {
    echo "$0:" "$@" >&2
}
die () {
    rc=$1
    shift
    warn "$@"
    exit "${rc}"
}

updateEnv(){
    echo "Update existing environment ${ENV_NAME}"
    NODE_GROUP=$(echo "${EXISTING_ENV_INFOS}" |jq '.nodeGroups[0].name'|  sed 's/\"//g');
    UPDATE_APP=$(curl  \
        -A "${USER_AGENT}" \
        -H "${CONTENT_TYPE}" \
        -X POST -fsS "$JELASTIC_URL/1.0/environment/control/rest/redeploycontainersbygroup" \
        --data "session=${SESSION}" --data "envName=${ENV_NAME}" --data "nodeGroup=${NODE_GROUP}" \
        --data "isSequential=true" \
        --data "tag=${DOCKER_IMAGE_TAG}" --data "login=$DOCKER_REGISTRY_USER" --data "password=$DOCKER_REGISTRY_PWD" --data "sequential=true");

    UPDATE_STATUS=$(echo "${UPDATE_APP}" | jq '.responses[0].result');
    if [[ ${UPDATE_STATUS} != "0" ]]
    then
        ERROR_MSG=$(echo "${UPDATE_APP}" | jq '.response.error');
        die 1 "Error in environment update: ${ERROR_MSG}";
    fi
    echo "Update environment done"
}


applyEnvGroups(){
    echo "Update env groups to  ${ENV_GROUPS}"

    UPDATE_GROUPS=$(curl  \
        -A "${USER_AGENT}" \
        -H "${CONTENT_TYPE}" \
        -X POST -fsS "$JELASTIC_URL/1.0/environment/control/rest/setenvgroup" \
        --data "session=${SESSION}" --data "envName=${ENV_NAME}" --data "envGroups=${ENV_GROUPS}");

    UPDATE_GROUPS_STATUS=$(echo "${UPDATE_GROUPS}" | jq '.result');
    if [[ ${UPDATE_GROUPS_STATUS} != "0" ]]
    then
        ERROR_GROUPS_MSG=$(echo "${UPDATE_GROUPS}" | jq '.error');
        die 1 "Error in groups update: ${ERROR_GROUPS_MSG}";
    fi
    echo "Update environment groups done to ${ENV_GROUPS}"
}


createEnv(){
    echo "Create new environment ${ENV_NAME}";

    #to escape / in values
    ENV_NAME_VALUE=$(echo "${ENV_NAME}" | sed 's/\//\\\//g')
    DOCKER_REGISTRY_URL_VALUE=$(echo "$DOCKER_REGISTRY_URL" | sed 's/\//\\\//g')
    DOCKER_REGISTRY_USER_VALUE=$(echo "$DOCKER_REGISTRY_USER" | sed 's/\//\\\//g')
    DOCKER_REGISTRY_PASSWORD_VALUE=$(echo "$DOCKER_REGISTRY_PWD" | sed 's/\//\\\//g')
    DOCKER_IMAGE_NAME_VALUE=$(echo "${DOCKER_IMAGE_NAME}" | sed 's/\//\\\//g')
    DOCKER_IMAGE_TAG_VALUE=$(echo "${DOCKER_IMAGE_TAG}" | sed 's/\//\\\//g')

    ENV_PROPS=$(sed -e "s/\${ENV_NAME}/$ENV_NAME_VALUE/"  env.json)

    NODES_PROPS=$(sed -e "s/\${DOCKER_REGISTRY_USER}/$DOCKER_REGISTRY_USER_VALUE/" \
                      -e "s/\${DOCKER_REGISTRY_PASSWORD}/$DOCKER_REGISTRY_PASSWORD_VALUE/" \
                      -e "s/\${DOCKER_REGISTRY_URL}/$DOCKER_REGISTRY_URL_VALUE/" \
                      -e "s/\${DOCKER_IMAGE_NAME}/$DOCKER_IMAGE_NAME_VALUE/" \
                      -e "s/\${DOCKER_IMAGE_TAG}/$DOCKER_IMAGE_TAG_VALUE/" nodes.json)

    INSTALL_APP=$(curl  \
        -A "${USER_AGENT}" \
        -H "${CONTENT_TYPE}" \
        -X POST -fsS "$JELASTIC_URL/1.0/environment/control/rest/createenvironment" \
        --data "session=${SESSION}" --data-urlencode "env=${ENV_PROPS}" --data-urlencode "nodes=${NODES_PROPS}");

    INSTALL_STATUS=$(echo "${INSTALL_APP}" | jq '.response.result');
    if [[ ${INSTALL_STATUS} != "0" ]]
    then
        ERROR_MSG=$(echo "${INSTALL_APP}" | jq '.response.error')
        die 1 "Error in environment creation: ${ERROR_MSG}";
    fi
    ENV_DOMAIN=$(echo "${INSTALL_APP}" | jq '.response.env.domain')
    echo "Install env on ${ENV_DOMAIN}"
}

signIn(){
    SIGNIN_JSON=$(curl -H "${CONTENT_TYPE}" -A "${USER_AGENT}" -X POST -fsS "$JELASTIC_URL/1.0/users/authentication/rest/signin"\
                       --data "login=$JELASTIC_LOGIN" --data "password=$JELASTIC_PWD" );

    SIGNIN_RESULT=$(echo "${SIGNIN_JSON}"|jq '.result');

    if [[ ${SIGNIN_RESULT} != "0" ]]
    then
     die 1 "ERROR in Sign In"
    fi

    SESSION=$(echo "${SIGNIN_JSON}"|jq '.session'|  sed 's/\"//g');
    echo "Sign In Done";
}

loadEnvironmentInfos(){
    #we test if the enviromnent exists
    EXISTING_ENV_INFOS=$(curl  \
      -A "${USER_AGENT}" \
      -H "${CONTENT_TYPE}" \
      -X POST -fsS "$JELASTIC_URL/1.0/environment/control/rest/getenvinfo" \
      --data "session=${SESSION}" --data "envName=${ENV_NAME}");

    ENV_EXIST=$(echo "${EXISTING_ENV_INFOS}"|jq '.result');
}

signIn
loadEnvironmentInfos
if [[ ${ENV_EXIST} != "0" ]]
then
    createEnv
else
    updateEnv
fi
applyEnvGroups
